package executor.scheduled;

import java.util.concurrent.*;

public class Example {
    public static void creatingInstance(){
        Callable<String> callableTask = () -> "Callable task";
        ScheduledExecutorService executorService =
                Executors.newSingleThreadScheduledExecutor();
        executorService.schedule(callableTask, 1, TimeUnit.SECONDS);
    }

    public static void delay(){
        Runnable runnableTask = () -> System.out.println("Callable task");
        ScheduledExecutorService service =
                Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnableTask, 100, 450, TimeUnit.MILLISECONDS);

        service.scheduleWithFixedDelay(runnableTask, 100, 150, TimeUnit.MILLISECONDS);
    }
}
