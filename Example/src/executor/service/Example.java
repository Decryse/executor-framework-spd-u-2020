package executor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Example {
    public static void createInstance(){
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.execute(() -> System.out.println("Asynchronous task"));

        executorService.shutdown();
    }

    public static void assigningTasks() throws ExecutionException, InterruptedException {
        Runnable runnableTask = () -> System.out.println("Runnable task");
        Callable<String> callableTask = () -> "Callable task";
        List<Callable<String>> callableTasks = new ArrayList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        executorService.execute(runnableTask);

        Future<String> future = executorService.submit(callableTask);

        String result = executorService.invokeAny(callableTasks);

        List<Future<String>> futures = executorService.invokeAll(callableTasks);
    }

    public static void terminatingStreams(){
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        executorService.shutdown();

        List<Runnable> notExecutedTasks = executorService.shutdownNow();

        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }

    }
}
