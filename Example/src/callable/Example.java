package callable;

import java.util.concurrent.*;

public class Example {
    public static void returnNumberAfterPause(ExecutorService executorService){
        Callable<Integer> task = () -> {
            TimeUnit.SECONDS.sleep(1);
            return 123;
        };
        Future<Integer> f = executorService.submit(task);
        try{
            System.out.println(f.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }

    }
}
