package runnable;

public class Example {

    public static void runnableExampleFromThePresentation(){
        /*
        Modern way to create Runnable task using lambda
         */
        Runnable task = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello " + threadName);
        };
        task.run();
        Thread thread = new Thread(task);
        thread.start();
        System.out.println("Done!");
    }

    public static void anonymousRunnable(){
        /*
        Modern way to create Runnable task using anonymous class
         */
        Runnable task = new Runnable(){
            @Override
            public void run(){
                String threadName = Thread.currentThread().getName();
                System.out.println("Hello " + threadName);
            }
        };
        task.run();
        Thread thread = new Thread(task);
        thread.start();
        System.out.println("Done!");
    }
}
