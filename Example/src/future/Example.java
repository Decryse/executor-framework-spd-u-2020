package future;

import java.util.concurrent.*;

public class Example {
    public static void takingResultImmediately(){
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Callable<String> callableTask = () -> "Callable task";
        Future<String> future = executorService.submit(callableTask);
        String result = null;
        try {
            result = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void takingResultAfterThreadExecution(){
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Callable<String> callableTask = () -> "Callable task";
        Future<String> future = executorService.submit(callableTask);
        String result = null;
        try {
            result = future.get(200, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public static void canceling(){
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Callable<String> callableTask = () -> "Callable task";
        Future<String> future = executorService.submit(callableTask);
        boolean canceled = future.cancel(true);
        boolean isCanceled = future.isCancelled();
    }
}
